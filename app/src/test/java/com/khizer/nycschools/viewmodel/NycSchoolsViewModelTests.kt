package com.khizer.nycschools.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.khizer.nycschools.getOrAwaitValue
import com.khizer.nycschools.model.NycSchoolScore
import com.khizer.nycschools.model.Resource
import com.khizer.nycschools.repository.NycSchoolsRepository
import com.khizer.nycschools.rules.CoroutinesTestRule
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.lang.Exception
import java.lang.RuntimeException


class NycSchoolsViewModelTests {
    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var testRepository: NycSchoolsRepository
    private lateinit var testViewModel: NycSchoolsViewModel

    @Before
    fun setup() {
        testRepository = mock {}
        testViewModel = NycSchoolsViewModel(testRepository)
    }

    @Test
    @ExperimentalCoroutinesApi
    fun testEmptyScoreForSchool() {
        coroutinesTestRule.testDispatcher.runBlockingTest {
            whenever(testRepository.fetchSchoolScores("")).thenCallRealMethod()
            testViewModel.findScoreForSchool("")
            val testScoreEmpty = testViewModel.schoolScores.getOrAwaitValue()
            assertThat(testScoreEmpty is Resource.Empty).isTrue()
        }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun testForExceptionCase() {
        coroutinesTestRule.testDispatcher.runBlockingTest {
            whenever(testRepository.fetchSchoolScores("abcd")) doThrow RuntimeException("Something went wrong")
            testViewModel.findScoreForSchool("abcd")

            pauseDispatcher()

            val testScoreLoading = testViewModel.schoolScores.getOrAwaitValue()
            assertThat(testScoreLoading is Resource.Loading).isTrue()

            resumeDispatcher()
            val testScoreException = testViewModel.schoolScores.getOrAwaitValue()
            val retrievedTestScore = (testScoreException as Resource.Error).throwable
            assertThat(retrievedTestScore.message).isEqualTo("Something went wrong")
        }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun testNotEmptyScoreForSchool() {
        coroutinesTestRule.testDispatcher.runBlockingTest {
            val testNycSchoolScore = NycSchoolScore("01M292",
                    "HENRY STREET SCHOOL", "29",
                    "355", "404", "363")

            whenever(testRepository.fetchSchoolScores(testNycSchoolScore.dbn)) doReturn listOf(testNycSchoolScore)
            testViewModel.findScoreForSchool(testNycSchoolScore.dbn)

            pauseDispatcher()

            val testScoreLoading = testViewModel.schoolScores.getOrAwaitValue()
            assertThat(testScoreLoading is Resource.Loading).isTrue()

            resumeDispatcher()

            val testScoreNotEmpty = testViewModel.schoolScores.getOrAwaitValue()
            val retrievedTestScore = (testScoreNotEmpty as Resource.Success<NycSchoolScore>).data
            assertThat(retrievedTestScore.schoolName).isEqualTo(testNycSchoolScore.schoolName)
        }
    }
}