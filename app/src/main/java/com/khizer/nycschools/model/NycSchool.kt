package com.khizer.nycschools.model

data class NycSchool(val dbn: String,
                     val schoolName: String,
                     val neighborhood: String,
                     val phoneNumber: String)