package com.khizer.nycschools.model

sealed class Resource<out T> {
    class Success<out T>(val data: T) : Resource<T>()
    class Loading<out T> : Resource<T>()
    class Empty<out T> : Resource<T>()
    class Error<out T>(val throwable: Throwable) : Resource<T>()
}