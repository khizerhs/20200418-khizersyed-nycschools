package com.khizer.nycschools.model

data class NycSchoolScore(val dbn: String,
                          val schoolName: String,
                          val numOfSatTestTakers: String,
                          val satCriticalReadingAvgScore: String,
                          val satMathAvgScore: String,
                          val satWritingAvgScore: String)