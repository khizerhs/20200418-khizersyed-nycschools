package com.khizer.nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagedList
import com.khizer.nycschools.model.NycSchool
import com.khizer.nycschools.model.NycSchoolScore
import com.khizer.nycschools.model.Resource
import com.khizer.nycschools.repository.NycSchoolsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class NycSchoolsViewModel @Inject constructor(private val nycSchoolsRepository: NycSchoolsRepository) : ViewModel() {
    var schools: LiveData<PagedList<NycSchool>>? = null

    private val _schoolScores = MutableLiveData<Resource<NycSchoolScore>>()
    val schoolScores: LiveData<Resource<NycSchoolScore>>
        get() = _schoolScores

    private val _progress = MutableLiveData<Resource<Unit>>()
    val progress: LiveData<Resource<Unit>>
        get() = _progress

    fun initializePaging() {
        val config = PagedList.Config.Builder()
                .setPageSize(50)
                .setInitialLoadSizeHint(50)
                .setEnablePlaceholders(false)
                .build()

        schools = nycSchoolsRepository.fetchNycSchools(config, viewModelScope, _progress)
    }

    fun findScoreForSchool(schoolDbn: String) {
        if (schoolDbn.isEmpty()) {
            _schoolScores.value = Resource.Empty()
        } else {
            _schoolScores.value = Resource.Loading()
            viewModelScope.launch(Dispatchers.IO) {
                try {
                    val schoolScoreList = nycSchoolsRepository.fetchSchoolScores(schoolDbn)
                    _schoolScores.postValue(
                            if (schoolScoreList.isEmpty()) Resource.Empty()
                            else Resource.Success(schoolScoreList.first())
                    )
                } catch (e: Exception) {
                    _schoolScores.postValue(Resource.Error(e))
                }
            }
        }
    }
}