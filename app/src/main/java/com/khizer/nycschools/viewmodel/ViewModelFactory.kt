package com.khizer.nycschools.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.khizer.nycschools.repository.NycSchoolsRepository
import dagger.Lazy
import javax.inject.Inject
import javax.inject.Provider

class ViewModelFactory @Inject constructor(private val nycSchoolsRepository: Provider<NycSchoolsRepository>): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T{
        return if (modelClass.isAssignableFrom(NycSchoolsViewModel::class.java)) {
            NycSchoolsViewModel(this.nycSchoolsRepository.get()) as T
        } else {
            throw IllegalArgumentException("ViewModel not found")
        }
    }
}