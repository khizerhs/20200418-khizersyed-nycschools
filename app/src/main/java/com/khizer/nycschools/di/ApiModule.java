package com.khizer.nycschools.di;

import androidx.annotation.NonNull;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.khizer.nycschools.api.NycSchoolsApi;
import com.khizer.nycschools.api.RequestInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {
    @Provides
    @Singleton
    @NonNull
    RequestInterceptor provideRequestInterceptor() {
        return new RequestInterceptor();
    }

    @Provides
    @Singleton
    @NonNull
    OkHttpClient provideOkHttpClient(final RequestInterceptor requestInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(requestInterceptor)
                .build();
    }

    @Provides
    @NonNull
    @Singleton
    GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create(new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create());
    }

    @Provides
    @Singleton
    @NonNull
    NycSchoolsApi provideNycApi(final OkHttpClient client,
                                final GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .baseUrl(NycSchoolsApi.BASE_URL)
                .client(client)
                .addConverterFactory(gsonConverterFactory)
                .build()
                .create(NycSchoolsApi.class);
    }
}
