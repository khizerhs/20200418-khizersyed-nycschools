package com.khizer.nycschools.di;

import android.content.Context;

import com.khizer.nycschools.ui.details.DetailsFragment;
import com.khizer.nycschools.ui.home.HomeFragment;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = {ApiModule.class})
public interface ApplicationComponent {
    @Component.Factory
    interface Factory {
        ApplicationComponent create(@BindsInstance Context Context);
    }

    void inject(HomeFragment homeFragment);
    void inject(DetailsFragment detailsFragment);
}
