package com.khizer.nycschools.api;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RequestInterceptor implements Interceptor {
    private static final String HEADER_APP_TOKEN = "X-App-Token";
    private static final String APP_TOKEN_VALUE = "WCz70Uzrq6SlsJUut82YgfhDT";

    private static final String QUERY_PARAM_ORDER = "$order";
    private static final String DBN_PROPERTY = "dbn";

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        final Request original = chain.request();
        final HttpUrl originalHttpUrl = original.url();
        final HttpUrl newUrl = originalHttpUrl.newBuilder()
                .addQueryParameter(QUERY_PARAM_ORDER, DBN_PROPERTY)
                .build();

        return chain.proceed(original.newBuilder()
                .url(newUrl)
                .header(HEADER_APP_TOKEN, APP_TOKEN_VALUE)
                .build());
    }
}
