package com.khizer.nycschools.api

import com.khizer.nycschools.model.NycSchoolScore
import com.khizer.nycschools.model.NycSchool
import retrofit2.http.GET
import retrofit2.http.Query

interface NycSchoolsApi {
    @GET("s3k6-pzi2.json")
    suspend fun getSchools(@Query("\$limit") limit: Int, @Query("\$offset") offset: Int): List<NycSchool>

    @GET("f9bf-2cp4.json")
    suspend fun getScoreForDbn(@Query("dbn") dbn: String): List<NycSchoolScore>

    companion object{
        const val BASE_URL = " https://data.cityofnewyork.us/resource/"
    }
}