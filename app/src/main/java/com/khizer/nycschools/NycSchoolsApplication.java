package com.khizer.nycschools;

import android.app.Application;

import com.khizer.nycschools.di.ApplicationComponent;
import com.khizer.nycschools.di.DaggerApplicationComponent;

public class NycSchoolsApplication extends Application {
    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerApplicationComponent.factory().create(getApplicationContext());
    }

    public ApplicationComponent getComponent() {
        return component;
    }
}
