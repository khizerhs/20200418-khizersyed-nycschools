package com.khizer.nycschools;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.khizer.nycschools.databinding.MainActivityBinding;
import com.khizer.nycschools.ui.details.DetailsFragment;
import com.khizer.nycschools.ui.home.HomeFragment;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(MainActivityBinding.inflate(getLayoutInflater()).getRoot());

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, HomeFragment.newInstance())
                    .commitNow();
        }
    }
}
