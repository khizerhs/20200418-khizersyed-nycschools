package com.khizer.nycschools.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.khizer.nycschools.api.NycSchoolsApi
import com.khizer.nycschools.model.NycSchool
import com.khizer.nycschools.model.Resource
import kotlinx.coroutines.CoroutineScope

class NycSchoolDataSourceFactory(private val api: NycSchoolsApi,
                                 private val scope: CoroutineScope,
                                 private val progress: MutableLiveData<Resource<Unit>>
) : DataSource.Factory<Int, NycSchool>() {
    override fun create(): DataSource<Int, NycSchool> {
        return NycSchoolDataSource(api, scope, progress)
    }

}