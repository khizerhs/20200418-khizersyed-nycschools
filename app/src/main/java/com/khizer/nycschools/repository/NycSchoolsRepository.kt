package com.khizer.nycschools.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.khizer.nycschools.api.NycSchoolsApi
import com.khizer.nycschools.model.NycSchool
import com.khizer.nycschools.model.NycSchoolScore
import com.khizer.nycschools.model.Resource
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

class NycSchoolsRepository @Inject constructor(private val nycSchoolsApi: NycSchoolsApi) {
    fun fetchNycSchools(config: PagedList.Config,
                        scope: CoroutineScope,
                        progress: MutableLiveData<Resource<Unit>>): LiveData<PagedList<NycSchool>> {
        val dataSourceFactory = NycSchoolDataSourceFactory(nycSchoolsApi, scope, progress)
        return LivePagedListBuilder(dataSourceFactory, config).build()
    }

    suspend fun fetchSchoolScores(dbn: String) = nycSchoolsApi.getScoreForDbn(dbn)
}