package com.khizer.nycschools.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.PositionalDataSource
import com.khizer.nycschools.api.NycSchoolsApi
import com.khizer.nycschools.model.NycSchool
import com.khizer.nycschools.model.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NycSchoolDataSource(private val api: NycSchoolsApi,
                          private val scope: CoroutineScope,
                          private val progress: MutableLiveData<Resource<Unit>>
) : PositionalDataSource<NycSchool>() {
    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<NycSchool>) {
        progress.postValue(Resource.Loading())
        scope.launch(Dispatchers.IO) {
            try {
                val schools = api.getSchools(params.loadSize, params.startPosition)
                callback.onResult(schools)
                progress.postValue(Resource.Success(Unit))
            } catch (e: Exception) {
                progress.postValue(Resource.Error(e))
            }
        }
    }

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<NycSchool>) {
        progress.postValue(Resource.Loading())
        scope.launch(Dispatchers.IO) {
            try {
                val schools = api.getSchools(params.pageSize, params.requestedStartPosition)
                callback.onResult(schools, params.requestedStartPosition, schools.size)
                progress.postValue(Resource.Success(Unit))
            } catch (e: Exception) {
                progress.postValue(Resource.Error(e))
            }
        }
    }
}