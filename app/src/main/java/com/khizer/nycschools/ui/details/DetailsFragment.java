package com.khizer.nycschools.ui.details;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.khizer.nycschools.NycSchoolsApplication;
import com.khizer.nycschools.R;
import com.khizer.nycschools.databinding.DetailsFragmentBinding;
import com.khizer.nycschools.model.NycSchoolScore;
import com.khizer.nycschools.model.Resource;
import com.khizer.nycschools.viewmodel.NycSchoolsViewModel;
import com.khizer.nycschools.viewmodel.ViewModelFactory;

import javax.inject.Inject;

public class DetailsFragment extends Fragment {
    @Inject
    ViewModelFactory vmFactory;

    private static final String KEY_NYC_SCHOOL_DBN = "nyc_school_dbn";

    private DetailsFragmentBinding binding;
    private NycSchoolsViewModel viewModel;

    public static DetailsFragment newInstance(final String schoolDbn) {
        final Bundle args= new Bundle();
        args.putString(KEY_NYC_SCHOOL_DBN, schoolDbn);
        final DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DetailsFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((NycSchoolsApplication) requireActivity().getApplication()).getComponent().inject(this);

        viewModel = new ViewModelProvider(requireActivity(), vmFactory)
                .get(NycSchoolsViewModel.class);

        setupObservers();

        viewModel.findScoreForSchool(requireArguments().getString(KEY_NYC_SCHOOL_DBN, ""));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void setupObservers() {
        viewModel.getSchoolScores().observe(getViewLifecycleOwner(), nycSchoolScore -> {
            if (nycSchoolScore instanceof Resource.Loading) {
                binding.progressBar.setVisibility(View.VISIBLE);
            } else if (nycSchoolScore instanceof Resource.Success) {
                final NycSchoolScore score = ((Resource.Success<NycSchoolScore>) nycSchoolScore).getData();
                showDetailsView(score);
            } else if (nycSchoolScore instanceof Resource.Empty) {
                binding.progressBar.setVisibility(View.GONE);
                binding.infoText.setVisibility(View.VISIBLE);
            } else {
                //Error
                binding.progressBar.setVisibility(View.GONE);
                binding.infoText.setVisibility(View.VISIBLE);
                binding.infoText.setText(R.string.error_txt);
            }
        });
    }


    private void showDetailsView(final NycSchoolScore nycSchoolScore) {
        binding.progressBar.setVisibility(View.GONE);
        binding.detailsGroup.setVisibility(View.VISIBLE);
        binding.name.setText(nycSchoolScore.getSchoolName());
        binding.numSatTakers.setText(nycSchoolScore.getNumOfSatTestTakers());
        binding.avgMathScore.setText(nycSchoolScore.getSatMathAvgScore());
        binding.avgWritingScore.setText(nycSchoolScore.getNumOfSatTestTakers());
        binding.avgReadingScore.setText(nycSchoolScore.getSatMathAvgScore());
    }
}
