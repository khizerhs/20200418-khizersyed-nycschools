package com.khizer.nycschools.ui.home;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;

import com.khizer.nycschools.databinding.ItemNycSchoolBinding;
import com.khizer.nycschools.model.NycSchool;

public class NycSchoolsAdapter extends PagedListAdapter<NycSchool, NycSchoolViewHolder> {
    private static final DiffUtil.ItemCallback<NycSchool> DIFF_CALLBACK = new NycSchoolDiffUtil();
    private SchoolClickListener schoolClickListener;

    public NycSchoolsAdapter(final SchoolClickListener schoolClickListener) {
        super(DIFF_CALLBACK);
        this.schoolClickListener = schoolClickListener;
    }

    @NonNull
    @Override
    public NycSchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final ItemNycSchoolBinding binding = ItemNycSchoolBinding.inflate(LayoutInflater.from(parent.getContext()));
        return new NycSchoolViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull NycSchoolViewHolder holder, int position) {
        holder.bind(getItem(position), schoolClickListener);
    }

}
