package com.khizer.nycschools.ui.home;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khizer.nycschools.databinding.ItemNycSchoolBinding;
import com.khizer.nycschools.model.NycSchool;

public class NycSchoolViewHolder extends RecyclerView.ViewHolder {
    private ItemNycSchoolBinding binding;

    public NycSchoolViewHolder(@NonNull ItemNycSchoolBinding nycSchoolBinding) {
        super(nycSchoolBinding.getRoot());
        this.binding = nycSchoolBinding;
    }

    public void bind(final NycSchool school,
                     final SchoolClickListener schoolClickListener) {
        binding.name.setText(school.getSchoolName());
        binding.phone.setText(school.getPhoneNumber());
        binding.neighborhood.setText(school.getNeighborhood());
        binding.getRoot().setOnClickListener(v -> {
            schoolClickListener.onSchoolClick(school.getDbn());
        });
    }
}
