package com.khizer.nycschools.ui.home;

public interface SchoolClickListener {
    void onSchoolClick(String schoolDbn);
}
