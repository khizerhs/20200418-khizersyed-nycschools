package com.khizer.nycschools.ui.home;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.khizer.nycschools.model.NycSchool;

public class NycSchoolDiffUtil extends DiffUtil.ItemCallback<NycSchool> {
    @Override
    public boolean areItemsTheSame(@NonNull NycSchool oldItem, @NonNull NycSchool newItem) {
        return oldItem.getDbn().equals(newItem.getDbn());
    }

    @Override
    public boolean areContentsTheSame(@NonNull NycSchool oldItem, @NonNull NycSchool newItem) {
        return oldItem.getDbn().equals(newItem.getDbn()) &&
                oldItem.getSchoolName().equals(newItem.getSchoolName()) &&
                oldItem.getNeighborhood().equals(newItem.getNeighborhood()) &&
                oldItem.getPhoneNumber().equals(newItem.getPhoneNumber());
    }
}
