package com.khizer.nycschools.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.khizer.nycschools.NycSchoolsApplication;
import com.khizer.nycschools.R;
import com.khizer.nycschools.databinding.HomeFragmentBinding;
import com.khizer.nycschools.model.Resource;
import com.khizer.nycschools.ui.details.DetailsFragment;
import com.khizer.nycschools.viewmodel.NycSchoolsViewModel;
import com.khizer.nycschools.viewmodel.ViewModelFactory;

import javax.inject.Inject;

public class HomeFragment extends Fragment implements SchoolClickListener {
    private HomeFragmentBinding binding;
    private NycSchoolsViewModel viewModel;

    @Inject
    ViewModelFactory vmFactory;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = HomeFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        ((NycSchoolsApplication) requireActivity().getApplication()).getComponent().inject(this);
        super.onActivityCreated(savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity(), vmFactory)
                .get(NycSchoolsViewModel.class);

        if (viewModel.getSchools() == null) {
            viewModel.initializePaging();
        }

        setupRecyclerView();
        setupObservers();
    }

    @Override
    public void onSchoolClick(final String schoolDbn) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.container, DetailsFragment.newInstance(schoolDbn))
                .addToBackStack(null)
                .commit();
    }

    private void setupRecyclerView() {
        binding.schoolsList.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.schoolsList.setAdapter(new NycSchoolsAdapter(this));
    }

    private void setupObservers() {
        viewModel.getSchools().observe(getViewLifecycleOwner(), nycSchools -> {
            final NycSchoolsAdapter schoolsAdapter = (NycSchoolsAdapter) binding.schoolsList.getAdapter();
            schoolsAdapter.submitList(nycSchools);
        });

        viewModel.getProgress().observe(getViewLifecycleOwner(), progress -> {
            if (progress instanceof Resource.Loading) {
                binding.progressBar.setVisibility(View.VISIBLE);
            } else if (progress instanceof Resource.Error) {
                binding.progressBar.setVisibility(View.GONE);
                Snackbar.make(binding.getRoot(), R.string.error_txt, BaseTransientBottomBar.LENGTH_LONG).show();
                Log.e("Khizer", ((Resource.Error) progress).getThrowable().getLocalizedMessage());
            } else if (progress instanceof Resource.Success) {
                binding.progressBar.setVisibility(View.GONE);
            }
        });
    }
}
