This code sample does two basic things. 
1. Display a list of NYC High Schools.
2. Selecting a school should show additional information about the school
	all the SAT scores - including Math, Reading and Writing.
